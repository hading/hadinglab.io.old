#!/bin/bash

zim --index content/notebook.zim

[ -e public/ ] && rm -rf public/

zim --export \
    --format=html --template=GitLabPagesTemplate \
    --output=./public \
    --overwrite \
    --verbose \
    --index-page=sitemap \
  content/notebook.zim

cp -a files/ public/
cp files/index.html public/index.html
cp files/robots.txt public/robots.txt

./build_sitemap.py

