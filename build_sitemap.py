#!/usr/bin/env python3

from html.parser import HTMLParser

class Parser(HTMLParser):

    def __init__(self, base_url, target_file):
        HTMLParser.__init__(self)
        self.urls = set()
        self.base_url = base_url
        self.target_file = target_file
        
    def handle_starttag(self, tag, attrs):
        if tag == "a":
            self.add_url(attrs)

    def add_url(self, attrs):
        attrs = dict(attrs)
        href = attrs.get("href", None)
        if href and href.startswith("."):
            self.urls.add(href.replace("./", "", 1))
            
    def emit(self):
        file = open(self.target_file, "w")
        for url in self.urls:
            print(self.base_url, "/", url, sep="", file=file)
        
parser = Parser("https://hading.gitlab.io", "public/sitemap.txt")
file = open("public/sitemap.html", "r")
parser.feed(file.read())
parser.emit()
